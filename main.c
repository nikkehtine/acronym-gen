#include <stdio.h>

int main()
{
    const int NUM_STRINGS = 10;
    const int STR_LENGTH;
    char** strings = malloc(NUM_STRINGS * sizeof(char*));
    if (strings == NULL) {
        printf("Insufficient memory available\n");
        return 1; // Or handle the error appropriately
    }
    for (int i = 0; i < NUM_STRINGS; i++) {
        strings[i] = malloc(64 * sizeof(char));
        if (strings[i] == NULL) {
            printf("Insufficient memory for string %d\n", i);
            printf("Your string should be %d characters long\n", i);
            return 1; // Or handle the error appropriately
        }
    }
    return 0;
}
